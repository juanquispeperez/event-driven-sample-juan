using System;

namespace Cips.API.Models
{
    public class CipDto
    {
        public Guid Cip { get; set; }
        public decimal Total  { get; set; }
        public string CurrencyCode { get; set; } 
        public string UserEmail { get; set; }
    }
}